# PhD Track of Computer Science

[TOC]


## Abstract

The PhD Track is a 5-years program (Master + PhD) that aims at
attracting excellent students interested in research. During the first
two years (i.e. during the Master), students attend courses, and
conduct a research project. A **tutor** advises the student during
their research project, and for selecting courses. At the end of the
Master, PhD track student that pass the **qualifying exam** can
continue for a PhD. The PhD advisor is typically the PhD Track tutor,
but it can be any other IPParis researcher.

The PhD Track program comes with three types of grants:

- **Full-package grants**: students receive a grant during their
  master (eg. 1000€ per month), and they do not pay IPParis student
  fees. If they pass the qualifying exam, students get a PhD grant
  (eg. 1800€ per month, depending on the type of PhD
  grant).

- **Master grants**: students receive a grant during their master
  (eg. 1000€ per month), and they do not pay IPParis student fees. To
  enter PhD, students (or advisors) need to get a PhD grant
  (eg. through IPParis PhD grant program).

- **No grant**: students do not receive any grant, but they not pay
  IPParis student fees.

## Schedule

- **September**: researchers that are willing to be **tutor** are identified.
- **December**: deadline for applying to the PhD track program
- **January-February**: selection of PhD Track students
  - The **PhD Track jury** assesses the PhD Track applicants. The best
    students are pre-selected and each student is assigned one or more
    potential tutors
  - The **Tutors** interview the students. The goal of the interview
    is to assess the student motivation and to define their research
    project.
  - The **PhD Track jury** ranks the phd track applicants and decides
    who gets accepted in the PhD Track program
- **March**: the Graduate School gathers the PhD track coordinators,
	and decides who gets a PhD track grant


## Contact 

## Head of the Graduate School
- [Hani Hamzeh \<hani.hamzeh@ip-paris.fr\>](mailto:hani.hamzeh@ip-paris.fr)

## Head of the Master/PhD Track of Computer Science
-  [François Trahay \<francois.trahay@telecom-sudparis.eu\>](mailto:francois.trahay@telecom-sudparis.eu)

## Head of the PhD Track of Data AI
- [Louis Jachiet](mailto:louis.jachiet@telecom-paris.fr) (DataAI)


## PhD Track contacts in IPParis labs

- [Florence Tupin\<florence.tupin@telecom-paris.fr\>](mailto:florence.tupin@telecom-paris.fr)
- [Sebastian Will\<sebastian.will@polytechnique.edu\>](mailto:sebastian.will@polytechnique.edu)
- [Natalia Kushik \<natalia.kushik@telecom-sudparis.eu\>](mailto:natalia.kushik@telecom-sudparis.eu)
- [Goran Frehse \<goran.frehse@ensta-paristech.fr\>](mailto:goran.frehse@ensta-paristech.fr)



### PhD Track jury

In Computer Science, the PhD track jury is composed of the head of the Master of Computer Science, and the heads of the master tracks:
- [François Trahay](mailto:francois.trahay@telecom-sudparis.eu)
- [Natalia Kushik](mailto:natalia.kushik@telecom-sudparis.eu) (CSN)
- [Stephane Maag](mailto:stephane.maag@telecom-sudparis.eu) (CSN)
- [Sergio Mover](mailto:sergio.mover@polytechnique.edu) (CPS)
- [Olivier Levillain](mailto:olivier.levillain@telecom-sudparis.eu) (Cyber)
- [Mehwish Alam](mehwish.alam@telecom-paris.fr) (DataAI)
- [Adriana Tapus](mailto:adriana.tapus@ensta-paris.fr) (DS4Health)
- [Samuel Mimram](mailto:samuel.mimram@lix.polytechnique.fr) (FCS)
- [Gilles Schaeffer](mailto:schaeffe@lix.polytechnique.fr) (FCS)
- [Amal Dev Parakkat](mailto:amal.parakkat@telecom-paris.fr) (IGD)
- [Pooran Memari](mailto:memari@lix.polytechnique.fr) (IGD)
- [Sourour Elloumi](mailto:sourour.elloumi@ensta-paris.fr) (MPRO)
- [Pierre Sutra](mailto:pierre.sutra@telecom-sudparis.eu) (PDS)
- [Gaël Thomas](mailto:gael.thomas@inria.fr) (PDS)

## Bureau IDIA

- [Olivier Bournez \<bournez@lix.polytechnique.fr\>](mailto:bournez@lix.polytechnique.fr)
- [Gilles Schaeffer \<schaeffe@lix.polytechnique.fr\>](mailto:schaeffe@lix.polytechnique.fr)
- [Talel Abdessalem \<talel.abdessalem@telecom-paris.fr\>](mailto:talel.abdessalem@telecom-paris.fr)
- [Francois Desbouvries \<francois.desbouvries@telecom-sudparis.eu\>](mailto:francois.desbouvries@telecom-sudparis.eu)
- [Maryline Laurent \<maryline.laurent@telecom-sudparis.eu\>](mailto:maryline.laurent@telecom-sudparis.eu)
- [Djamal Zeghlache \<djamal.zeghlache@telecom-sudparis.eu\>](mailto:djamal.zeghlache@telecom-sudparis.eu)
- [Amel Bouzeghoub \<amel.bouzeghoub@telecom-sudparis.eu\>](mailto:amel.bouzeghoub@telecom-sudparis.eu)
- [Benjamin Doerr \<doerr@lix.polytechnique.fr\>](mailto:doerr@lix.polytechnique.fr)
- [Thomas Bonald \<thomas.bonald@telecom-paris.fr\>](mailto:thomas.bonald@telecom-paris.fr)
- [Eric Goubault \<goubault@lix.polytechnique.fr\>](mailto:goubault@lix.polytechnique.fr)
- [David Filliat \<david.filliat@ensta-paris.fr\>](mailto:david.filliat@ensta-paris.fr)
- [Goran Frehse \<goran.frehse@ensta-paris.fr\>](mailto:goran.frehse@ensta-paris.fr)
- [François Fages \<francois.fages@inria.fr\>](mailto:francois.fages@inria.fr)
- [Samuel Mimram \<samuel.mimram@lix.polytechnique.fr\>](mailto:samuel.mimram@lix.polytechnique.fr)
- [Titus Zaharia \<titus.zaharia@telecom-sudparis.eu\>](mailto:titus.zaharia@telecom-sudparis.eu)
- [Djamel Belaid \<Djamel.Belaid@telecom-sudparis.eu\>](mailto:Djamel.Belaid@telecom-sudparis.eu)
- [Florence Tupin \<florence.tupin@telecom-paris.fr\>](mailto:florence.tupin@telecom-paris.fr)
- [François Trahay \<francois.trahay@telecom-sudparis.eu\>](mailto:francois.trahay@telecom-sudparis.eu)
- [Romain Alléaume \<romain.alleaume@telecom-paris.fr\>](mailto:romain.alleaume@telecom-paris.fr)
- [Ada Diaconescu \<ada.diaconescu@telecom-paristech.fr\>](mailto:ada.diaconescu@telecom-paristech.fr)
