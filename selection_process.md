# Selection process for the PhD Track of Computer Science

[TOC]

IPParis defines the selection process [as presented in these
slides](guide_PhD_Track_2023-2024.pdf). For the PhD track of Computer
Science, we implement this process in the following way.

### Phase 1: Admission


A Jury for each PhD Track examines the applications and
select eligible candidates.

**Who is involved ?**: the heads of the master tracks, and the head of the master.

**How does it work ?**
Applications are evaluated on [the HotCRP website](https://cs.ip-paris.fr/hotcrp/phd23/).

#### Sorting applications

First, you can sort the applications by assigning one or more tags to each file:

- `csn, cps, cyber, dataai, ds4h, fcs, mpro, pds, igd`: the application should be evaluated by the jury of track `X`
  - Evolution since last year:
    - the `hpda` track does not exist anymore: it has been merged into the `pdf` track
    - the `ds4h` track was created. Adriana is in charge of this track, named [Digital Skills for Health Transformation](https://www.ip-paris.fr/en/education/masters/computer-science-program/master-year-2-digital-skills-health-transformation-ds4health)

  - an application may be evaluated by several tracks.  For example, a student wanting to do AI applied to cybersecurity might be tagged `[cyber]` and `[dataai]̀

- `math`: for students on the Phd track DataAI who are to be integrated into the Maths master's programme

You can now add/remove tags to the various folders to assign them to the different routes.


#### Evaluation of applications

You can start writing reviews for the applications assigned to your
respective tracks. The deadline for evaluating all applications is
February 2.

You can find the list of your applications by searching for your tag,
for example `#pds` for the PDS track.  For each application, you
should assign several reviewers (at least 2, ideally 3). To do this,
click on `assign reviews`, select the reviewers the reviewers and
choose the Round `Review`, then click on `Save assignments`.

If reviewers are missing, you can invite them from the `[Users]` section (https://cs.ip-paris.fr/hotcrp/phd23/users/all).

The reviewers will then have to evaluate the applications. Among the
information to be provided in the review is the name of a tutor (from an
IPParis lab) for the student. This can be a tutor mentioned
by the student in his or her letter, or a tutor working on the
candidate's research project.

* * *

### Phase 2: assigning tutors

The Jury forwards eligible applications to the departments which,
based on the student's profile and motivations in relation to a
research project with a research project and in consultation with the
laboratories, chooses potential tutors. Tutors must have a good
knowledge of the proposed courses of study, so as to be able to the
student's academic progress.


In Computer Science, the bureau IDIA meets in extended formation, and
is responsible for the assignment of tutors. The admissions jury may
suggest a tutor or tutors on the basis of the candidate's application
(for (e.g. when a candidate wishes to work with a particular person).
particular person).


**Who is involved ?**: The bureau IDIA, PhD Track contacts in the
labs, the heads of the PhD Track of Computer Science, and DataAI.

* * *

### Phase 3: students interview

The Phd Track contacts in the labs contact the tutors, and send them
the applications. They explain the interviewing process to the tutors.

The tutors contact the student, and schedule an interview. The goal of
this interview is to assess and support them in defining their
research project and educational path.

After the interview, the tutor fills a [Fiche projet](Fiche_projet.docx).

**Who is involved ?**: PhD Track contacts in the labs, tutors who were
assigned students during phase 2.

#### Student interview

The interview has two goals:

- define the candidate's research project and identify the tutor
  best suited to supervise him/her
  
- complete the candidate's assessment so that we can rank the
  candidates in phase 4.


To this end, the tutor(s) have access to the candidate's file and to the assessments
assessments (see phase 1). The tutor contacts the candidate by e-mail
to arrange an interview. Following the interview, the
tutor fills in a [Project form](Fiche_projet.docx) containing:


- **Appréciation du candidat**: the assessment will be used to
  candidates, who are probably all of a good standard (as weak
  applications should have been eliminated during phase 1). We
  therefore ask you to judge the candidate's level as follows:

  - *Insufficient*: the candidate's level is insufficient for the
    PhD Track program. The tutor recommends not to accept this
    student in the PhD Track.

  - *Good*: the candidate has a good level. The tutor recommends to
    accept this candidate in PhD Track **and commits to supervise**
    him/her in PhD Track.

  - *Very good*: the candidate is very good. Among all the candidates,
    this candidate is in the top 30%. The tutor recommends to accept
    this candidate in PhD Track **and commits to supervise** him/her in
    PhD Track.

  - *Exceptional*: the candidate is exceptional. Among all the candidates, this
    candidate is in the top 10%. The tutor recommends
    to accept this candidate in PhD Track **and commits to supervise** him/her in PhD Track.

- **Track**: if the student is accepted in the PhD Track program, they will attend courses, and will be integrated in one of the [tracks of the Master of Computer Science](https://cs.ip-paris.fr/master/):
  - CSN (Computer Science for Networks)
  - CPS	(Cyber-Physical Systems)
  - Cyber (Cybersecurity)
  - DataAI (Data and Artificial Intelligence)
  - DS4Health (Digital Skills for Health Transformation)
  - FCS (Foundations of Computer Science/MPRI)
  - IGD (Interaction, Graphics & Design)
  - MPRO (Operations Research)
  - PDS (Parallel and Distributed Systems)


- A **tutor** who may be different from the person who conducted the
  who conducted the interview. In this case, the tutor must be
  consulted and **commit to supervising the student** if he/she is
  accepted in PhD Track (see [Role of the tutor](#r%C3%B4le-de-la-tutricetuteur))
  
- **PhD Track grants**: Since funding is limited, not all the accepted
  students will get a phd track grant. A tutor may commit to fund the
  student grant (master grant and/or phd grant) using their own
  funding (eg. through an ANR project).

* * *

### Phase 4: ranking applicants

The assessment of the tutors are discussed by the Jury. The Jury
deliberates in a concerted and collegial manner, to decide on the list
of admitted candidates, and establish an inter-ranking between all the
candidates, without ex aequo, before communicating it to the Graduate
School. Director of the Master.

**Who is involved ?**: the heads of the master tracks, the head of the
phd tracks of Computer Science, and DataAI, the PhD Track contacts in
the labs, and the Bureau IDIA.

* * *


### Phase 5: CcER validation

The Graduate School in charge of the PhD Track checks the admitted
students, and the CcER validates the list of admitted students.

**Who is involved ?**:  the [head of the graduate school](mailto:hani.hamzeh@ip-paris.fr), the [head of the PhD Track of Computer Science](mailto:francois.trahay@telecom-sudparis.eu), and the [head of the PhD Track of DataAI](mailto:louis.jachiet@telecom-paris.fr)

* * *


## Schedule

Phase 1: admission
- 13/1: application deadline for the students
- 13/1 - 19/1: dispatch phase (the track chair assign students to the tracks and PC members to students)
- 19/1 at 2pm: first meeting of the PhD track jury (check that all the assignments are corrects)
	- **who?** heads of the tracks + head of the master
- 19/1 - 2/2: reviewing phase (each track reviews its students)
- 2/2 at 2pm: second meeting of the PhD track jury (decide which applications we accept, and at which level)
	- **who?** heads of the tracks + head of the master

Phase 2: attribution des tuteurs.tutrices
- 2/2 - 7/2: analysis by the IDIA bureau + référents laboratoires
- 7/2 at 2pm: first meeting of the IDIA bureau (check that we agree with the tutors/students for the interviews)
	- **who?** bureau IDIA

Phase 3: entretiens
- 7/2 - 26/2: interview phase
	- **who?** tutors

Phase 4: interclassement
- 26/2 at 2pm: second meeting of the IDIA bureau (harmonization)
	- **who?** bureau IDIA + heads of the tracks

Phase 5: validation par le CcER
- 28/2: deadline for the graduate school

* * *

## Roles

### Roles of the tutors

- The identity of the tutor must be known as early as the admission
  phase. The tutor will then be informed of his or her role (and
  whether or not to accept it), and of his or her obligations to the
  student.  The tutor will commit to  supervising the student during the Master;

- The tutor contacts the student before his/her arrival on arrival on
  campus or at the beginning of September, in order to draw up the
  "contrat pédagogique" with the student, the head of the master's
  track and the pedagogical secretary;
  
- Once admitted, a phd track student conducts research under the
  supervision of the tutor;

- The tutor will meet with the PhD track student regularly, at least
  once a month, in order to assess the student performance during
  courses, and to advise them on their research project;
  
- The tutor will fill the phd track student evaluation form (at the
  end of M1, and in the middle of M2), in collaboration with the head
  of the phd track, and the head of the student master track;


- The tutor may change during the phd track, as the student research
  project evolves. This change has to be valided by the head of the
  PhD Track, and the Graduate School.


- The tutor may become the student phd advisor, or not.

### Head of the PhD Track

The role of the head of a PhD Track is to:

- propose the composition of the Jury for the admission phase of the
  PhD Track for which it is responsible, and to organize and steer
  this phase - in in collaboration with Jury members, track managers,
  etc;

- manage the ranking of all candidates (M1 and
  M2) proposed for admission to the PhD Track program, together with a
  a proposal for the type of scholarship to be awarded to the best-placed
  for submission to the scholarship arbitration committee
  ;

- manage, and synchronize evaluation of phd track students;

- participate in the collection of data and the compilation of
  success statistics for PhD Track students;

- validate, with the person in charge of the PhD Track Program at the
  Graduate School, the list of tutors for each admitted student.
  students - in May/June of the year preceding the start of the next
  academic year academic year, and to direct tutors to the Master's
  program management in the event of difficulties;

- help manage special cases: dropping out of school, gaps in studies
  school drop-out, gap year, repetition, academic academic/medical/other
  difficulties, changes of course/level, etc.

- monitor students throughout their studies PhD Track - in
  collaboration with the teaching teams, pedagogical and the person in
  charge of the PhD Track program at the Graduate School.

### PhD Track contacts in the labs

Make the link between successful candidates and the researchers who
will be interviewing them (if applicable, they do the interviewing
themselves).

They intervene after the selection phase and before the interviews.

* * *
